package com.oy.gulimall.member.exception;

/**
 * @Author OY
 * @Date 2021/11/8
 */
public class UserExistException extends RuntimeException{

    public UserExistException(){
        super("存在相同的用户名");
    }
}
