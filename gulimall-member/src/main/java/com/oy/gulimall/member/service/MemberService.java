package com.oy.gulimall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.oy.gulimall.common.utils.PageUtils;
import com.oy.gulimall.member.entity.MemberEntity;
import com.oy.gulimall.member.vo.GiteeUser;
import com.oy.gulimall.member.vo.MemberUserLoginVo;
import com.oy.gulimall.member.vo.SocialUser;
import com.oy.gulimall.member.vo.UserRegisterVo;

import java.util.Map;

/**
 * 会员
 *
 * @author oy
 * @email oy2097291754@gmail.com
 * @date 2021-04-18 01:15:12
 */
public interface MemberService extends IService<MemberEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 功能描述: 注册
     * @param vo
     */
    void register(UserRegisterVo vo);

    /**
     * 功能描述：登录
     * @param vo
     * @return
     */
    MemberEntity login(MemberUserLoginVo vo);

    /**
     * Gitee 登录
     * @param vo
     * @return
     */
    MemberEntity login(GiteeUser vo) throws Exception;

    /**
     * 社交用户的登录
     * @param socialUser
     * @return
     */
    MemberEntity login(SocialUser socialUser) throws Exception;
}

