package com.oy.gulimall.member.vo;

import lombok.Data;

/**
 * @Author OY
 * @Date 2021/11/16
 */
@Data
public class GiteeUser {
    private String access_token;
    private String token_type;
    private long expires_in;
    private String refresh_token;
    private String scope;
    private long created_at;
}
