package com.oy.gulimall.member.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.oy.gulimall.common.utils.HttpUtils;
import com.oy.gulimall.member.entity.MemberLevelEntity;
import com.oy.gulimall.member.exception.PhoneNumExistException;
import com.oy.gulimall.member.exception.UserExistException;
import com.oy.gulimall.member.service.MemberLevelService;
import com.oy.gulimall.member.vo.GiteeUser;
import com.oy.gulimall.member.vo.MemberUserLoginVo;
import com.oy.gulimall.member.vo.SocialUser;
import com.oy.gulimall.member.vo.UserRegisterVo;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.oy.gulimall.common.utils.PageUtils;
import com.oy.gulimall.common.utils.Query;

import com.oy.gulimall.member.dao.MemberDao;
import com.oy.gulimall.member.entity.MemberEntity;
import com.oy.gulimall.member.service.MemberService;


@Service("memberService")
public class MemberServiceImpl extends ServiceImpl<MemberDao, MemberEntity> implements MemberService {

    @Autowired
    private MemberLevelService memberLevelService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<MemberEntity> page = this.page(
                new Query<MemberEntity>().getPage(params),
                new QueryWrapper<MemberEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public void register(UserRegisterVo vo) {
        // 1. 检查电话号是否唯一
        checkPhoneUnique(vo.getPhone());
        // 2. 检查用户名是否唯一
        checkUserNameUnique(vo.getUserName());
        // 3. 该用户信息唯一，进行插入
        MemberEntity entity = new MemberEntity();
        entity.setUsername(vo.getUserName());
        entity.setMobile(vo.getPhone());
        entity.setCreateTime(new Date());
        // 3.2 使用加密保存密码
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encodePassword = passwordEncoder.encode(vo.getPassword());
        entity.setPassword(encodePassword);
        // 3.3 设置会员默认等级
        // 3.3.1 找到会员默认等级
        MemberLevelEntity defaultLevel = memberLevelService.getOne(new QueryWrapper<MemberLevelEntity>().eq("default_status", 1));
        // 3.3.2 设置会员等级
        entity.setLevelId(defaultLevel.getId());

        // 4.保存用户信息
        this.save(entity);

    }

    @Override
    public MemberEntity login(MemberUserLoginVo vo) {
        String loginnacct = vo.getLoginacct();
        // 以用户名或电话号登录的进行查询
        MemberEntity entity = this.getOne(new QueryWrapper<MemberEntity>().eq("username", loginnacct).or().eq("mobile", loginnacct));
        if(entity != null){
            BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
            boolean matches = bCryptPasswordEncoder.matches(vo.getPassword(), entity.getPassword());
            if(matches){
                entity.setPassword("");
                return entity;
            }
        }
        return null;
    }

    @Override
    public MemberEntity login(GiteeUser vo) throws Exception {
        //1、查询当前社交用户的社交账号信息（昵称、性别等）
        Map<String,String> query = new HashMap<>();
        query.put("access_token",vo.getAccess_token());
        HttpResponse response = HttpUtils.doGet("https://gitee.com", "/api/v5/user", "get", new HashMap<String, String>(), query);
        if (response.getStatusLine().getStatusCode() == 200) {
            //查询成功
            String json = EntityUtils.toString(response.getEntity());
            JSONObject jsonObject = JSON.parseObject(json);
            String name = jsonObject.getString("name");
            String avatar_url = jsonObject.getString("avatar_url");
            String uid = jsonObject.getString("id");
            String email = jsonObject.getString("email");

            MemberEntity memberEntity = this.baseMapper.selectOne(new QueryWrapper<MemberEntity>().eq("social_uid", uid));
            if(memberEntity != null){
                //这个用户已经注册过
                //更新用户的访问令牌的时间和access_token
                MemberEntity update = new MemberEntity();
                update.setId(memberEntity.getId());
                update.setAccessToken(vo.getAccess_token());
                update.setExpiresIn(vo.getExpires_in());
                this.baseMapper.updateById(update);

                memberEntity.setAccessToken(vo.getAccess_token());
                memberEntity.setExpiresIn(vo.getExpires_in());
                return memberEntity;
            }else{
                //2、没有查到当前社交用户对应的记录我们就需要注册一个
                MemberEntity register = new MemberEntity();
                register.setNickname(name);
                register.setHeader(avatar_url);
                register.setCreateTime(new Date());
                register.setSocialUid(uid);
                register.setAccessToken(vo.getAccess_token());
                register.setExpiresIn(vo.getExpires_in());
                register.setEmail(email);

                //把用户信息插入到数据库中
                this.baseMapper.insert(register);
                return register;
            }
        }
        return null;
    }

    @Override
    public MemberEntity login(SocialUser socialUser) throws Exception {
        //具有登录和注册逻辑
        String uid = socialUser.getUid();

        //1、判断当前社交用户是否已经登录过系统
        MemberEntity memberEntity = this.baseMapper.selectOne(new QueryWrapper<MemberEntity>().eq("social_uid", uid));

        if (memberEntity != null) {
            //这个用户已经注册过
            //更新用户的访问令牌的时间和access_token
            MemberEntity update = new MemberEntity();
            update.setId(memberEntity.getId());
            update.setAccessToken(socialUser.getAccess_token());
            update.setExpiresIn(socialUser.getExpires_in());
            this.baseMapper.updateById(update);

            memberEntity.setAccessToken(socialUser.getAccess_token());
            memberEntity.setExpiresIn(socialUser.getExpires_in());
            return memberEntity;
        } else {
            //2、没有查到当前社交用户对应的记录我们就需要注册一个
            MemberEntity register = new MemberEntity();
            //3、查询当前社交用户的社交账号信息（昵称、性别等）
            Map<String,String> query = new HashMap<>();
            query.put("access_token",socialUser.getAccess_token());
            query.put("uid",socialUser.getUid());
            HttpResponse response = HttpUtils.doGet("https://api.weibo.com", "/2/users/show.json", "get", new HashMap<String, String>(), query);

            if (response.getStatusLine().getStatusCode() == 200) {
                //查询成功
                String json = EntityUtils.toString(response.getEntity());
                JSONObject jsonObject = JSON.parseObject(json);
                String name = jsonObject.getString("name");
                String gender = jsonObject.getString("gender");
                String profileImageUrl = jsonObject.getString("profile_image_url");

                register.setNickname(name);
                register.setGender("m".equals(gender)?1:0);
                register.setHeader(profileImageUrl);
                register.setCreateTime(new Date());
                register.setSocialUid(socialUser.getUid());
                register.setAccessToken(socialUser.getAccess_token());
                register.setExpiresIn(socialUser.getExpires_in());

                //把用户信息插入到数据库中
                this.baseMapper.insert(register);

            }
            return register;
        }
    }

    private void checkUserNameUnique(String userName){
        Integer count = baseMapper.selectCount(new QueryWrapper<MemberEntity>().eq("username", userName));
        if(count > 0){
            throw new UserExistException();
        }
    }

    private void checkPhoneUnique(String phone){
        Integer count = baseMapper.selectCount(new QueryWrapper<MemberEntity>().eq("mobile", phone));
        if(count > 0){
            throw new PhoneNumExistException();
        }

    }

}