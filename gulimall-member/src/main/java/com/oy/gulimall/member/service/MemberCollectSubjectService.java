package com.oy.gulimall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.oy.gulimall.common.utils.PageUtils;
import com.oy.gulimall.member.entity.MemberCollectSubjectEntity;

import java.util.Map;

/**
 * 会员收藏的专题活动
 *
 * @author oy
 * @email oy2097291754@gmail.com
 * @date 2021-04-18 01:15:12
 */
public interface MemberCollectSubjectService extends IService<MemberCollectSubjectEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

