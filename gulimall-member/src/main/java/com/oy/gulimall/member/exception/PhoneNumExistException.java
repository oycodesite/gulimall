package com.oy.gulimall.member.exception;

/**
 * @Author OY
 * @Date 2021/11/8
 */
public class PhoneNumExistException extends RuntimeException{
    public PhoneNumExistException(){
        super("存在相同的手机号");
    }
}
