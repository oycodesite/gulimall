package com.oy.gulimall.member.vo;

import lombok.Data;

/**
 * @Author OY
 * @Date 2021/11/10
 */
@Data
public class MemberUserLoginVo {

    private String loginacct;

    private String password;
}
