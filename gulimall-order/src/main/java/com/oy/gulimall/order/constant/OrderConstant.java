package com.oy.gulimall.order.constant;

/**
 * @Author OY
 * @Date 2022/1/9
 */
public class OrderConstant {

    public static final String USER_ORDER_TOKEN_PREFIX = "order:token";
}
