package com.oy.gulimall.order.service.impl;

import com.alibaba.fastjson.TypeReference;
import com.oy.gulimall.common.to.SkuHasStockVo;
import com.oy.gulimall.common.utils.R;
import com.oy.gulimall.common.vo.MemberResponseVo;
import com.oy.gulimall.order.constant.OrderConstant;
import com.oy.gulimall.order.feign.CartFeignService;
import com.oy.gulimall.order.feign.MemberFeignService;
import com.oy.gulimall.order.feign.WmsFeignService;
import com.oy.gulimall.order.interceptor.LoginInterceptor;
import com.oy.gulimall.order.vo.MemberAddressVo;
import com.oy.gulimall.order.vo.OrderConfirmVo;
import com.oy.gulimall.order.vo.OrderItemVo;
import com.oy.gulimall.order.vo.SkuStockVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.oy.gulimall.common.utils.PageUtils;
import com.oy.gulimall.common.utils.Query;

import com.oy.gulimall.order.dao.OrderDao;
import com.oy.gulimall.order.entity.OrderEntity;
import com.oy.gulimall.order.service.OrderService;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;


@Service("orderService")
public class OrderServiceImpl extends ServiceImpl<OrderDao, OrderEntity> implements OrderService {


    @Autowired
    private ThreadPoolExecutor executor;

    @Autowired
    private CartFeignService cartFeignService;

    @Autowired
    private MemberFeignService memberFeignService;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private WmsFeignService wmsFeignService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<OrderEntity> page = this.page(
                new Query<OrderEntity>().getPage(params),
                new QueryWrapper<OrderEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public OrderConfirmVo confirmOrder() throws ExecutionException, InterruptedException {
        MemberResponseVo memberResponseVo = LoginInterceptor.loginUser.get();
        OrderConfirmVo orderConfirmVo = new OrderConfirmVo();

        //TODO :获取当前线程请求头信息(解决Feign异步调用丢失请求头问题)
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();

        // 开启第一个异步任务
        CompletableFuture<Void> addressFuture = CompletableFuture.runAsync(() -> {

            // 每个线程都来共享之前的请求数据
            RequestContextHolder.setRequestAttributes(requestAttributes);

            // 1、远程查询所有的收获地址列表
            List<MemberAddressVo> address = memberFeignService.getAddress(memberResponseVo.getId());
            orderConfirmVo.setMemberAddressVos(address);
        }, executor);


        CompletableFuture<Void> cartInfoFuture = CompletableFuture.runAsync(() -> {

            // 每个线程都来功享之前的请求数据
            RequestContextHolder.setRequestAttributes(requestAttributes);

            // 2. 查出所有选中购物项
            List<OrderItemVo> checkedItems = cartFeignService.getCurrentCartItems();
            orderConfirmVo.setItems(checkedItems);
            // fegin在远程调用之前要构造请求，调用很多的拦截器
        }, executor).thenRunAsync(() -> {
            List<OrderItemVo> items = orderConfirmVo.getItems();
            // 获取全部商品的id
            List<Long> skuIds = items.stream().map((itemVo -> itemVo.getSkuId())).collect(Collectors.toList());

            // 远程查询商品信息
            R skuHasStock = wmsFeignService.getSkuHasStock(skuIds);
            List<SkuStockVo> skuStockVos = skuHasStock.getData("data", new TypeReference<List<SkuStockVo>>() {});

            if(skuStockVos != null && skuStockVos.size() > 0){
                // 将skuStockVos集合转换为map
                Map<Long,Boolean> skuHasStockMap = skuStockVos.stream().collect(Collectors.toMap(SkuStockVo::getSkuId,SkuStockVo::getHasStock));
                orderConfirmVo.setStocks(skuHasStockMap);
            }
        },executor);

        // 3、查询用户积分
        Integer integration = memberResponseVo.getIntegration();
        orderConfirmVo.setIntegeration(integration);

        // 4、价格数据自动计算

        // TODO 5、防重令牌(防止表单重复提交)
        // 为用户设置一个token，三十分钟过期时间（存在redis）
        String token = UUID.randomUUID().toString().replace("-", "");
        redisTemplate.opsForValue().set(OrderConstant.USER_ORDER_TOKEN_PREFIX+memberResponseVo.getId(),token,30, TimeUnit.MINUTES);
        orderConfirmVo.setOrderToken(token);

        CompletableFuture.allOf(addressFuture,cartInfoFuture).get();

        return orderConfirmVo;
    }

}