package com.oy.gulimall.order.dao;

import com.oy.gulimall.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author oy
 * @email oy2097291754@gmail.com
 * @date 2021-04-18 01:10:21
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {
	
}
