package com.oy.gulimall.auth.controller;

import com.alibaba.fastjson.TypeReference;
import com.oy.gulimall.auth.feign.MemberFeignService;
import com.oy.gulimall.auth.feign.ThirdPartFeignService;
import com.oy.gulimall.auth.vo.UserLoginVo;
import com.oy.gulimall.auth.vo.UserRegisterVo;
import com.oy.gulimall.common.constant.AuthServerConstant;
import com.oy.gulimall.common.exception.BizCodeEnum;
import com.oy.gulimall.common.utils.R;
import com.oy.gulimall.common.vo.MemberResponseVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import static com.oy.gulimall.common.constant.AuthServerConstant.LOGIN_USER;

/**
 * @Author OY
 * @Date 2021/10/31
 */
@Controller
@Slf4j
public class LoginController {

    @Autowired
    private ThirdPartFeignService thirdPartFeignService;

    @Autowired
    private MemberFeignService memberFeignService;

    @Autowired
    private StringRedisTemplate redisTemplate;
//    @GetMapping("/login.html")
//    public String loginPage(){
//        return "login";
//    }
//
//    @GetMapping
//    public String regPage(){
//        return "reg";
//    }

    @GetMapping(value = "/sms/sendCode")
    @ResponseBody
    public R sendCode(@RequestParam("phone")String phone){
        // 接口防刷，在redis中缓存phone-code
        ValueOperations<String,String> ops = redisTemplate.opsForValue();
        String prePhone = AuthServerConstant.SMS_CODE_CACHE_PREFIX + phone;
        String v = ops.get(prePhone);
        if(!StringUtils.isEmpty(v)){
            long pre = Long.parseLong(v.split("_")[1]);
            // 如果存储时间小于60s，说明60s内发送过验证码
            if(System.currentTimeMillis() - pre < 6000){
                return R.error(BizCodeEnum.SMS_CODE_EXCEPTION.getCode(),BizCodeEnum.SMS_CODE_EXCEPTION.getMsg());
            }
        }

        // 如果存在的话，删除之前的验证码
        redisTemplate.delete(prePhone);
        // 获取到6位数字的验证码
        String code = String.valueOf((int) ((Math.random() + 1) * 100000));
        // 在redis中进行存储并设置过期时间
        ops.set(prePhone,code+"_"+System.currentTimeMillis(),1, TimeUnit.MINUTES);
        thirdPartFeignService.sendCode(phone,code);
        return R.ok();
    }

    @PostMapping(value = "/register")
    public String register(@Valid UserRegisterVo registerVo, BindingResult result, RedirectAttributes attributes){
        // 1. 判断校验是否通过
        HashMap<String, String> errors = new HashMap<>();
        if(result.hasErrors()){
            // 1.1 如果校验不通过，则封装校验结果
            result.getFieldErrors().forEach(item -> {
                errors.put(item.getField(),item.getDefaultMessage());
                // 1.2 将错误信息封装session中
                attributes.addFlashAttribute("errors",errors);
            });
            // 1.2. 重定向到注册页
            return "redirect:http://auth.gulimall.com/reg.html";
        }else{
            // 2. JSR303校验通过
            // 判断验证码是否正确
            String code = redisTemplate.opsForValue().get(AuthServerConstant.SMS_CODE_CACHE_PREFIX + registerVo.getPhone());
            // 2.1 如果对应手机的验证码不为空与提交上的相等-》验证码正确
            if(!StringUtils.isEmpty(code) && registerVo.getCode().equals(code.split("_")[0])){
                // 2.1.1 使得对应的验证码失效
                redisTemplate.delete(AuthServerConstant.SMS_CODE_CACHE_PREFIX+registerVo.getPhone());

                //2.1.2 远程调用会员服务注册
                R r = memberFeignService.register(registerVo);
                if(r.getCode() == 0){
                    // 调用成功，重定向登录页
                    log.info("登录成功");
                    return "redirect:http://auth.gulimall.com/login.html";
                }else{
                    // 调用失败，返回注册并显示错误信息
                    String msg = (String) r.get("msg");
                    errors.put("msg",msg);
                    attributes.addFlashAttribute("errors",errors);
                    return "redirect:http://auth.gulimall.com/reg.html";
                }
            }else{
                // 2.2 验证码错误
                errors.put("code","验证码错误");
                attributes.addFlashAttribute("errors",errors);
                return "redirect:http://auth.gulimall.com/reg.html";
            }
        }
    }

    @PostMapping(value = "/login")
    public String login(UserLoginVo vo, RedirectAttributes attributes, HttpSession session){

        // 远程登录
        R login = memberFeignService.login(vo);

        if(login.getCode() == 0){
            MemberResponseVo data = login.getData("data", new TypeReference<MemberResponseVo>() {
            });
            session.setAttribute(LOGIN_USER,data);
            return "redirect:http://gulimall.com";
        }else{
            HashMap<String, String> errors = new HashMap<>();
            errors.put("msg",login.getData("msg",new TypeReference<String>(){}));
            attributes.addFlashAttribute("errors",errors);
            return "redirect:http://auth.gulimall.com/login.html";
        }
    }

    @GetMapping(value = "/login.html")
    public String loginPage(HttpSession session){

        // 从session先取出来用户的信息，判断用户是否已经登录过了
        Object attribute = session.getAttribute(LOGIN_USER);
        if(attribute == null){
            return "login";
        }else{
            return "redirect:http://gulimall.com";
        }
    }
}
