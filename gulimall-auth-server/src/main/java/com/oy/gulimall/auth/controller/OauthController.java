package com.oy.gulimall.auth.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.oy.gulimall.auth.feign.MemberFeignService;
import com.oy.gulimall.auth.vo.GiteeUser;
import com.oy.gulimall.auth.vo.SocialUser;
import com.oy.gulimall.common.utils.HttpUtils;
import com.oy.gulimall.common.utils.R;
import com.oy.gulimall.common.vo.MemberResponseVo;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import java.util.HashMap;

import static com.oy.gulimall.common.constant.AuthServerConstant.LOGIN_USER;

/**
 * @Author OY
 * @Date 2021/11/13
 */
@Controller
@RequestMapping(value = "/oauth2.0")
public class OauthController {

    @Autowired
    private MemberFeignService memberFeignService;

    @RequestMapping(value = "/weibo/success")
    public String authorize(String code, RedirectAttributes attributes, HttpSession session) throws Exception {
        // 1. 使用code换取token，换取成功则继续，否则重定向至登录页
        HashMap<String, String> query = new HashMap<>();
        query.put("client_id","3768935929");
        query.put("client_secret","ccd3689364ddd5d928d00896f22930e8");
        query.put("grant_type","authorization_code");
        query.put("redirect_uri","http://auth.gulimall.com/oauth2.0/weibo/success");
        query.put("code",code);
        // 发送post请求换取token
        HttpResponse response = HttpUtils.doPost("https://api.weibo.com",
                "/oauth2/access_token",
                "post",
                new HashMap<String, String>(), query,
                new HashMap<String, String>());
        HashMap<String, String> errors = new HashMap<>();
        if(response.getStatusLine().getStatusCode() == 200){
            // 2. 调用member远程接口进行oauth登录，登录成功则转发至首页并携带返回用户信息，否则转发至登录页
            String json = EntityUtils.toString(response.getEntity());
            SocialUser socialUser = JSON.parseObject(json, new TypeReference<SocialUser>() {
            });
            R login = memberFeignService.oauthLogin(socialUser);
            if(login.getCode() == 0){
                String jsonString = JSON.toJSONString(login.get("data"));
                MemberResponseVo memberResponseVo = JSON.parseObject(jsonString, new TypeReference<MemberResponseVo>() {});
                attributes.addFlashAttribute("user",memberResponseVo);
                // 1、第一次使用session，命令浏览器保存卡号，JSESSIONID这个cookie
                // 以后浏览器访问哪个网站就会带上这个网站的cookie
                // TODO  1、默认发的令牌。当前域（解决子域session共享问题）
                // TODO 2、使用JSON的序列化方式来序列化对象到Redis中
                session.setAttribute(LOGIN_USER,memberResponseVo);

                return "redirect:http://gulimall.com";
            }else{
                // 2.2 否则返回登录页
                errors.put("msg","登录失败,请重试");
                attributes.addFlashAttribute("errors",errors);
                return "redirect:http://auth.gulimall.com/login.html";
            }
        }else{
            errors.put("msg","获取第三方授权失败，请重试");
            attributes.addFlashAttribute("errors",errors);
            return "redirect:http://auth.gulimall.com/login.html";
        }
    }

    /**
     *  Gitee登录
     * @param code
     * @param attributes
     * @param session
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/gitee/success")
    public String authorizeGitee(@RequestParam(value = "code") String code, RedirectAttributes attributes, HttpSession session) throws Exception {
        // 1. 使用code换取token，换取成功则继续，否则重定向至登录页
        HashMap<String, String> query = new HashMap<>();
        query.put("grant_type","authorization_code");
        query.put("client_id","d3ee6cc6534726c8d8ce2d21cb54ad474c2d2af32407712ad25a6d0c59b48d7f");
        query.put("client_secret","a82bccba2bcc017128c0720fe7290241b3c891cfe03ea6d0579db44294f0f186");
        query.put("redirect_uri","http://auth.gulimall.com/oauth2.0/gitee/success");
        query.put("code",code);
        // 发送post请求换取token
        HttpResponse response = HttpUtils.doPost("https://gitee.com",
                "/oauth/token",
                "post",
                new HashMap<String, String>(), query,
                new HashMap<String, String>());
        HashMap<String, String> errors = new HashMap<>();
        System.out.println(response);
        if(response.getStatusLine().getStatusCode() == 200){
            // 2. 调用member远程接口进行oauth登录，登录成功则转发至首页并携带返回用户信息，否则转发至登录页
            String json = EntityUtils.toString(response.getEntity());
            GiteeUser giteeUser = JSON.parseObject(json, new TypeReference<GiteeUser>() {
            });
            R login = memberFeignService.oauthGiteeLogin(giteeUser);
            if(login.getCode() == 0){
                String jsonString = JSON.toJSONString(login.get("data"));
                MemberResponseVo memberResponseVo = JSON.parseObject(jsonString, new TypeReference<MemberResponseVo>() {});
                attributes.addFlashAttribute("user",memberResponseVo);
                // 1、第一次使用session，命令浏览器保存卡号，JSESSIONID这个cookie
                // 以后浏览器访问哪个网站就会带上这个网站的cookie
                // TODO  1、默认发的令牌。当前域（解决子域session共享问题）
                // TODO 2、使用JSON的序列化方式来序列化对象到Redis中
                session.setAttribute(LOGIN_USER,memberResponseVo);

                return "redirect:http://gulimall.com";
            }else{
                // 2.2 否则返回登录页
                errors.put("msg","登录失败,请重试");
                attributes.addFlashAttribute("errors",errors);
                return "redirect:http://auth.gulimall.com/login.html";
            }
        }else{
            errors.put("msg","获取第三方授权失败，请重试");
            attributes.addFlashAttribute("errors",errors);
            return "redirect:http://auth.gulimall.com/login.html";
        }
    }

}
