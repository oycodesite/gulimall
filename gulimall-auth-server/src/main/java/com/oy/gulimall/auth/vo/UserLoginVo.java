package com.oy.gulimall.auth.vo;

import lombok.Data;

/**
 * @Author OY
 * @Date 2021/11/10
 */
@Data
public class UserLoginVo {

    private String loginacct;

    private String password;
}
