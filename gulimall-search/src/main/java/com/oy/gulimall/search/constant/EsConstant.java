package com.oy.gulimall.search.constant;

/**
 * @Author OY
 * @Date 2021/7/26
 */
public class EsConstant {
    // 在es中的索引
    public static final String PRODUCT_INDEX = "mall_product";
    public static final int PRODUCT_PAGESIZE = 10;
}
