package com.oy.gulimall.search.service;

import com.oy.gulimall.common.to.es.SkuEsModel;

import java.io.IOException;
import java.util.List;

/**
 * @Author OY
 * @Date 2021/7/26
 */
public interface ProductSaveService {
    /**
     * 功能描述：上架商品
     * @param skuEsModels
     * @return
     */
    boolean productStatusUp(List<SkuEsModel> skuEsModels) throws IOException;
}
