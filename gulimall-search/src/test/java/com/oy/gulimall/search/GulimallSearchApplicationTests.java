package com.oy.gulimall.search;

import com.alibaba.fastjson.JSON;
import com.oy.gulimall.search.config.GulimallElasticSearch;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.Avg;
import org.elasticsearch.search.aggregations.metrics.AvgAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.io.IOException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GulimallSearchApplicationTests {

    @Autowired
    private RestHighLevelClient client;

    @Data
    static class Account {
        private int account_number;
        private int balance;
        private String firstname;
        private String lastname;
        private int age;
        private String gender;
        private String address;
        private String employer;
        private String email;
        private String city;
        private String state;

        @Override
        public String toString() {
            return "Account{" +
                    "account_number=" + account_number +
                    ", balance=" + balance +
                    ", firstname='" + firstname + '\'' +
                    ", lastname='" + lastname + '\'' +
                    ", age=" + age +
                    ", gender='" + gender + '\'' +
                    ", address='" + address + '\'' +
                    ", employer='" + employer + '\'' +
                    ", email='" + email + '\'' +
                    ", city='" + city + '\'' +
                    ", state='" + state + '\'' +
                    '}';
        }
    }

    @Test
    public void contextLoads() {
        System.err.println(client);
    }

    /**
     * 聚合查询
     * @throws IOException
     */
    @Test
    public void find1() throws IOException {
        // 1. 创建索引请求
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices("bank");
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        // 构造检索条件
//        sourceBuilder.query();
//        sourceBuilder.from();
//        sourceBuilder.size();
//        sourceBuilder.aggregation();
        sourceBuilder.query(QueryBuilders.matchQuery("address","mill"));
        // AggergationBuilders工具类AggregationBuilder
        // 构建第一个聚合条件：按照年龄的值分布
        TermsAggregationBuilder agg1 = AggregationBuilders.terms("ageAgg").field("age").size(10); //聚合名称
        // 参数为AggregationBuilder
        sourceBuilder.aggregation(agg1);
        // 构建第二个聚合条件：平均薪资
        AvgAggregationBuilder agg2 = AggregationBuilders.avg("balanceAvg").field("balance");
        sourceBuilder.aggregation(agg2);

        System.err.println("检索条件"+sourceBuilder.toString());
        searchRequest.source(sourceBuilder);
        // 2. 执行检索
        SearchResponse response = client.search(searchRequest,GulimallElasticSearch.COMMON_OPTIONS);
        // 3. 分析响应结果
       // System.err.println(response.toString());
        // 3.1 获取java bean
//        SearchHits hits = response.getHits();
//        SearchHit[] hits1 = hits.getHits();
//
//        for(SearchHit hit : hits1){
//            hit.getId();
//            hit.getIndex();
//            String sourceAsString = hit.getSourceAsString();
//            Account account = JSON.parseObject(sourceAsString,Account.class);
//            System.err.println(account);
//        }
        // 3.2 获取检索到的分析信息
        Aggregations aggregations = response.getAggregations();
        Terms ageAgg = aggregations.get("ageAgg");
        for (Terms.Bucket bucket : ageAgg.getBuckets()) {
            System.out.println("年龄：" + bucket.getKeyAsString() + "--人数： " + bucket.getDocCount());
        }
        Avg balanceAvg = aggregations.get("balanceAvg");
        System.err.println("薪资平均值:"+balanceAvg.getValue());

    }

    @Test
    public void find() throws IOException {
        // 1. 创建索引请求
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices("bank");
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        // 构造检索条件
//        sourceBuilder.query();
//        sourceBuilder.from();
//        sourceBuilder.size();
//        sourceBuilder.aggregation();
        sourceBuilder.query(QueryBuilders.matchQuery("address","mill"));
        System.err.println(sourceBuilder.toString());

        searchRequest.source(sourceBuilder);

        // 2. 执行检索
        SearchResponse response = client.search(searchRequest, GulimallElasticSearch.COMMON_OPTIONS);
        // 3 分析响应结果
        System.err.println(response.toString());
    }

    @Test
    public void indexData() throws IOException {
        // 设置索引
        IndexRequest indexRequest = new IndexRequest("user");
        indexRequest.id("1"); // 数据id

        User user = new User();
        user.setUserName("张三");
        user.setAge(20);
        user.setGender("男");
        String jsonString = JSON.toJSONString(user);

        // 设置要保存的内容，指定数据和类型
        indexRequest.source(jsonString, XContentType.JSON);

        // 执行创建索引和保存数据
        IndexResponse index = client.index(indexRequest, GulimallElasticSearch.COMMON_OPTIONS);

        System.err.println();

    }


    @Getter
    @Setter
    class User {
        private String userName;
        private String gender;
        private Integer age;
    }

}
