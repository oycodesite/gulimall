package com.oy.gulimall.product.vo;

/**
 * @Author OY
 * @Date 2021/10/14
 */

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class SkuItemSaleAttrVo {

    private Long attrId;

    private String attrName;

    private List<AttrValueWithSkuIdVo> attrValues;
//    // 属性值
//    private String attrValue;
//    // 该属性值对应的skuId的集合
//    private String skuIds;
}
