package com.oy.gulimall.product.exception;

import com.oy.gulimall.common.exception.BizCodeEnum;
import com.oy.gulimall.common.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author OY
 * @Date 2021/5/2
 */
@Slf4j
@RestControllerAdvice(basePackages = "com.oy.gulimall.product.controller") // 管理的controller
public class GulimallExceptionControllerAdvice {

    @ExceptionHandler(value = Exception.class) // 也可以返回ModelAndView
    public R handleValidException(MethodArgumentNotValidException exception){

        Map<String, String> map = new HashMap<>();
        // 获取数据校验的错误结果
        BindingResult bindingResult = exception.getBindingResult();
        // 处理结果
        bindingResult.getFieldErrors().forEach(fieldError -> {
            String massage = fieldError.getDefaultMessage();
            String field = fieldError.getField();
            map.put(field,massage);
        });

        log.error("数据校验出现问题{},异常类型{}",exception.getMessage(),exception.getClass());

        return R.error(400,"数据校验错误出现问题").put("data",map);
    }

    @ExceptionHandler(value = Throwable.class) // 异常更大
    public R handleException(Throwable throwable){
        log.error("未知异常{},异常类型{}",
                throwable.getMessage(),
                throwable.getClass());
        return R.error(BizCodeEnum.UNKNOW_EXCEPTION.getCode(),BizCodeEnum.UNKNOW_EXCEPTION.getMsg());
    }
}
