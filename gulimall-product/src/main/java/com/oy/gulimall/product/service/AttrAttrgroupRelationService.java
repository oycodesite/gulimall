package com.oy.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.oy.gulimall.common.utils.PageUtils;
import com.oy.gulimall.product.entity.AttrAttrgroupRelationEntity;
import com.oy.gulimall.product.vo.AttrGroupRelationVo;

import java.util.List;
import java.util.Map;

/**
 * 属性&属性分组关联
 *
 * @author oy
 * @email oy2097291754@gmail.com
 * @date 2021-04-18 17:29:28
 */
public interface AttrAttrgroupRelationService extends IService<AttrAttrgroupRelationEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveBathch(List<AttrGroupRelationVo> vos);
}

