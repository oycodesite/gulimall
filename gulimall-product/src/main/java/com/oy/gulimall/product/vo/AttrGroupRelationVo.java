package com.oy.gulimall.product.vo;

import lombok.Data;

/**
 * @Author OY
 * @Date 2021/5/3
 */
@Data
public class AttrGroupRelationVo {

    // "attrId":1,"attrGroupId":2
    private Long attrId;
    private Long attrGroupId;
}
