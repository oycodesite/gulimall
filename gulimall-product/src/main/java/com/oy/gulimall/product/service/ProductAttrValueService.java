package com.oy.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.oy.gulimall.common.utils.PageUtils;
import com.oy.gulimall.product.entity.ProductAttrValueEntity;
import com.oy.gulimall.product.vo.BaseAttrs;
import com.oy.gulimall.product.vo.SpuItemAttrGroupVo;

import java.util.List;
import java.util.Map;

/**
 * spu属性值
 *
 * @author oy
 * @email oy2097291754@gmail.com
 * @date 2021-04-18 17:29:28
 */
public interface ProductAttrValueService extends IService<ProductAttrValueEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveProductAttr(List<ProductAttrValueEntity> baseAttrs);

    List<ProductAttrValueEntity> baseAttrlistforspu(Long spuId);

    void updateSpuAttr(Long spuId, List<ProductAttrValueEntity> entities);

    List<SpuItemAttrGroupVo> getProductGroupAttrsBySpuId(Long spuId, Long catalogId);
}

