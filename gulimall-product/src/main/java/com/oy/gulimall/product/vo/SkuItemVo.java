package com.oy.gulimall.product.vo;

import com.oy.gulimall.product.entity.SkuImagesEntity;
import com.oy.gulimall.product.entity.SkuInfoEntity;
import com.oy.gulimall.product.entity.SpuInfoDescEntity;
import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * @Author OY
 * @Date 2021/10/14
 */
@ToString
@Data
public class SkuItemVo {
    // 1、sku基本信息的获取 pms_sku_info
    private SkuInfoEntity info;

    private boolean hashStock = true;

    // 2、sku的图片信息 pms_sku_images
    private List<SkuImagesEntity> images;

    // 3、获取spu的销售属性组合
    private List<SkuItemSaleAttrVo> saleAttr;

    // 4、 获取spu的介绍
    private SpuInfoDescEntity desc;

    // 5、获取spu的规格参数信息
    private List<SpuItemAttrGroupVo> groupAttrs;
}
