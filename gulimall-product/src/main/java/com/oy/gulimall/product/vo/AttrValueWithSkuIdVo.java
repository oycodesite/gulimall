package com.oy.gulimall.product.vo;

import lombok.Data;
import lombok.ToString;

/**
 * @Author OY
 * @Date 2021/10/15
 */
@Data
@ToString
public class AttrValueWithSkuIdVo {
    // 属性值
    private String attrValue;
    // 该属性值对应的skuId的集合
    private String skuIds;
}
