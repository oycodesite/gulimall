package com.oy.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.oy.gulimall.common.utils.PageUtils;
import com.oy.gulimall.product.entity.CommentReplayEntity;

import java.util.Map;

/**
 * 商品评价回复关系
 *
 * @author oy
 * @email oy2097291754@gmail.com
 * @date 2021-04-18 17:29:28
 */
public interface CommentReplayService extends IService<CommentReplayEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

