package com.oy.gulimall.product.vo;

/**
 * @Author OY
 * @Date 2021/5/3
 */

import com.oy.gulimall.product.entity.AttrEntity;
import lombok.Data;

@Data
public class AttrVo extends AttrEntity {

    private Long attrGroupId;
}
