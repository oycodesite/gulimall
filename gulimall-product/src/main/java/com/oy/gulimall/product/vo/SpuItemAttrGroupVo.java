package com.oy.gulimall.product.vo;

import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * @Author OY
 * @Date 2021/10/14
 */
@Data
@ToString
public class SpuItemAttrGroupVo {
    private String groupName;
    // attrId,attrName,attrValue
    private List<Attr> attrs;
}
