package com.oy.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.oy.gulimall.common.utils.PageUtils;
import com.oy.gulimall.product.entity.CategoryEntity;
import com.oy.gulimall.product.vo.Catalog2Vo;

import java.util.List;
import java.util.Map;

/**
 * 商品三级分类
 *
 * @author oy
 * @email oy2097291754@gmail.com
 * @date 2021-04-18 17:29:28
 */
public interface CategoryService extends IService<CategoryEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<CategoryEntity> listWithTree();

    void removeMenuById(List<Long> asList);

    Long[] findCateLogPath(Long catelogId);

    List<CategoryEntity> getLevel1Catagories();

    Map<String, List<Catalog2Vo>> getCategoryMap();

    Map<String, List<Catalog2Vo>> getCatalogJson();
}

