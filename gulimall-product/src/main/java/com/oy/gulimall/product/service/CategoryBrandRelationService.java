package com.oy.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.oy.gulimall.common.utils.PageUtils;
import com.oy.gulimall.product.entity.BrandEntity;
import com.oy.gulimall.product.entity.CategoryBrandRelationEntity;

import java.util.List;
import java.util.Map;

/**
 * 品牌分类关联
 *
 * @author oy
 * @email oy2097291754@gmail.com
 * @date 2021-04-18 17:29:28
 */
public interface CategoryBrandRelationService extends IService<CategoryBrandRelationEntity> {



    PageUtils queryPage(Map<String, Object> params);

    void saveDetail(CategoryBrandRelationEntity categoryBrandRelation);

    void updateBrand(Long brandId, String name);

    List<BrandEntity> getBrandByCatId(Long catId);
}

