package com.oy.gulimall.ware.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author OY
 * @Date 2022/1/11
 */
@Data
public class FareVo {
    private MemberAddressVo addressVo;
    private BigDecimal fare;
}
