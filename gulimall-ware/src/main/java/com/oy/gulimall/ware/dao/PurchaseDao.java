package com.oy.gulimall.ware.dao;

import com.oy.gulimall.ware.entity.PurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author oy
 * @email oy2097291754@gmail.com
 * @date 2021-04-18 01:18:38
 */
@Mapper
public interface PurchaseDao extends BaseMapper<PurchaseEntity> {
	
}
