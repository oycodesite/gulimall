package com.oy.gulimall.ware.vo;

import lombok.Data;

/**
 * @Author OY
 * @Date 2021/7/23
 */
@Data
public class SkuHasStockVo {
    private Long skuId;

    private Boolean hasStock;
}
