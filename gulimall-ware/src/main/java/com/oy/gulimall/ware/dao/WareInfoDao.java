package com.oy.gulimall.ware.dao;

import com.oy.gulimall.ware.entity.WareInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author oy
 * @email oy2097291754@gmail.com
 * @date 2021-04-18 01:18:38
 */
@Mapper
public interface WareInfoDao extends BaseMapper<WareInfoEntity> {
	
}
