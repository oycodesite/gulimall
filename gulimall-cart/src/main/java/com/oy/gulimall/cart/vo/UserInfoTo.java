package com.oy.gulimall.cart.vo;

import lombok.Data;

/**
 * @Author OY
 * @Date 2021/12/1
 */
@Data
public class UserInfoTo {

    private Long userId;

    private String userKey;

    /**
     * 是否临时用户
     */
    private Boolean tempUser = false;
}
