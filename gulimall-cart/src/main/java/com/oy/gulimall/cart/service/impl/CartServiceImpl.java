package com.oy.gulimall.cart.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.oy.gulimall.cart.exception.CartExceptionHandler;
import com.oy.gulimall.cart.feign.ProductFeignService;
import com.oy.gulimall.cart.interceptor.CartInterceptor;
import com.oy.gulimall.cart.service.CartService;
import com.oy.gulimall.cart.vo.CartItemVo;
import com.oy.gulimall.cart.vo.CartVo;
import com.oy.gulimall.cart.vo.SkuInfoVo;
import com.oy.gulimall.cart.vo.UserInfoTo;
import com.oy.gulimall.common.constant.CartConstant;
import com.oy.gulimall.common.utils.R;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.stream.Collectors;

/**
 * @Author OY
 * @Date 2021/12/19
 */
@Service
public class CartServiceImpl implements CartService {

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private ProductFeignService productFeignService;

    @Autowired
    private ThreadPoolExecutor executor;


    public CartItemVo addToCart(Long skuId, Integer num) throws Exception {

        // 拿到要操作的购物车信息
        BoundHashOperations<String, Object, Object> cartOps = getCartOps();

        String productRedisValues = (String) cartOps.get(skuId.toString());

        // 如果没有就添加数据
        if(StringUtils.isEmpty(productRedisValues)){

            // 2、添加新的商品到购物车（redis）
            CartItemVo cartItemVo = new CartItemVo();
            // 开启第一个异步任务
            CompletableFuture<Void> getSkuInfoFuture = CompletableFuture.runAsync(() -> {
                // 1、添加新的商品到购物车（redis）
                R productSkuInfo = productFeignService.getInfo(skuId);
                SkuInfoVo skuInfo = productSkuInfo.getData("skuInfo", new TypeReference<SkuInfoVo>() {
                });
                // 数据复制操作
                cartItemVo.setSkuId(skuInfo.getSkuId());
                cartItemVo.setTitle(skuInfo.getSkuTitle());
                cartItemVo.setImage(skuInfo.getSkuDefaultImg());
                cartItemVo.setPrice(skuInfo.getPrice());
                cartItemVo.setCount(num);
            }, executor);

            // 开启第二个异步任务
            CompletableFuture<Void> getSkuAttrValuesFuture = CompletableFuture.runAsync(() -> {
                // 2. 远程查询skuAttrValues组合信息
                List<String> skuSaleAttrValues = productFeignService.getSkuSaleAttrValues(skuId);
                cartItemVo.setSkuAttrValues(skuSaleAttrValues);
            }, executor);

            // 等待所有的异步任务全部完成
            CompletableFuture.allOf(getSkuInfoFuture,getSkuAttrValuesFuture).get();

            String cartItemJson = JSON.toJSONString(cartItemVo);
            cartOps.put(skuId.toString(),cartItemJson);

            return cartItemVo;
        }else{
            // 购物车有此商品，修改数量即可
            CartItemVo cartItemVo = JSON.parseObject(productRedisValues, CartItemVo.class);
            cartItemVo.setCount(cartItemVo.getCount() + num);
            // 修改redis的数据
            String cartItemJson = JSON.toJSONString(cartItemVo);
            cartOps.put(skuId.toString(),cartItemJson);

            return cartItemVo;
        }

    }

    private BoundHashOperations<String, Object, Object> getCartOps() {
        // 先得到当前用户信息
        UserInfoTo userInfoTo = CartInterceptor.threadLocal.get();

        String cartKey = "";
        if(userInfoTo.getUserId() != null){
            // gulimall:cart:1
            cartKey = CartConstant.CART_PREFIX + userInfoTo.getUserId();
        } else {
            cartKey = CartConstant.CART_PREFIX + userInfoTo.getUserKey();
        }

        // 绑定指定的key操作Redis
        BoundHashOperations<String, Object, Object> operations = redisTemplate.boundHashOps(cartKey);

        return operations;
    }

    @Override
    public CartVo getCart() throws Exception {
        CartVo cartVo = new CartVo();
        UserInfoTo userInfoTo = CartInterceptor.threadLocal.get();
        if(userInfoTo.getUserId() != null){
            // 1. 登录
            String cartKey = CartConstant.CART_PREFIX + userInfoTo.getUserId();
            // 临时购物车的键
            String temptCartKey = CartConstant.CART_PREFIX + userInfoTo.getUserKey();

            // 2、如果临时购物车的数据还未进行合并
            List<CartItemVo> tempCartItems = getCartItems(temptCartKey);
            if(tempCartItems != null){
                // 临时临时车有数据需要进行合并操作
                for (CartItemVo item: tempCartItems) {
                    addToCart(item.getSkuId(),item.getCount());
                }

                // 清除临时购物车的数据
                clearCartInfo(temptCartKey);
            }
            // 3、获取登录后的购物车数据【包含合并过来的临时购物车的数据和登录后购物车的数据】
            List<CartItemVo> cartItems = getCartItems(cartKey);
            cartVo.setItems(cartItems);
        }else{
            // 没登录
            String cartKey = CartConstant.CART_PREFIX + userInfoTo.getUserKey();
            // 获取临时购物车里面的所有购物项
            List<CartItemVo> cartItems = getCartItems(cartKey);
            cartVo.setItems(cartItems);
        }
        return cartVo;
    }

    public List<CartItemVo> getCartItems(String cartKey){
        // 获取购物车里面的所有商品
        BoundHashOperations<String, Object, Object> operations = redisTemplate.boundHashOps(cartKey);
        List<Object> values = operations.values();
        if(values != null && values.size() > 0){
            List<CartItemVo> cartItemVoStream = values.stream().map((obj) -> {
                String str = (String) obj;
                CartItemVo cartItemVo = JSON.parseObject(str, CartItemVo.class);
                return cartItemVo;
            }).collect(Collectors.toList());
            return cartItemVoStream;
        }
        return null;
    }

    @Override
    public void clearCartInfo(String cartKey) {
        redisTemplate.delete(cartKey);
    }

    @Override
    public CartItemVo getCartItem(Long skuId) {
        // 拿到要操作的购物车信息
        BoundHashOperations<String, Object, Object> cartOps = getCartOps();

        String redisValue = (String) cartOps.get(skuId.toString());

        CartItemVo cartItemVo = JSON.parseObject(redisValue, CartItemVo.class);

        return cartItemVo;
    }

    @Override
    public void checkItem(Long skuId, Integer checked) {

        // 查询购物车里面的的商品
        CartItemVo cartItem = getCartItem(skuId);

        // 修改商品状态
        cartItem.setCheck(checked == 1 ? true:false);

        // 序列化存入redis
        String redisValue = JSON.toJSONString(cartItem);

        BoundHashOperations<String, Object, Object> cartOps = getCartOps();
        cartOps.put(skuId.toString(),redisValue);

    }

    @Override
    public void changeItemCount(Long skuId, Integer num) {
        //查询购物车里面的商品
        CartItemVo cartItem = getCartItem(skuId);
        cartItem.setCount(num);

        BoundHashOperations<String, Object, Object> cartOps = getCartOps();
        //序列化存入redis中
        String redisValue = JSON.toJSONString(cartItem);
        cartOps.put(skuId.toString(),redisValue);
    }

    @Override
    public void deleteIdCartInfo(Integer skuId) {
        BoundHashOperations<String, Object, Object> cartOps = getCartOps();
        cartOps.delete(skuId.toString());
    }

    @Override
    public List<CartItemVo> getUserCartItems() {

        List<CartItemVo> cartItemVos = new ArrayList<>();
        // 获取当前用户登录的信息
        UserInfoTo userInfoTo = CartInterceptor.threadLocal.get();
        // 获取当前用户登录直接返回null
        if(userInfoTo.getUserId() == null){
            return null;
        }else{
            // 获取购物车项
            String cartKey = CartConstant.CART_PREFIX + userInfoTo.getUserId();
            // 获取所有的
            List<CartItemVo> cartItems = getCartItems(cartKey);
            if(cartItems == null){
                throw new CartExceptionHandler();
            }

            // 筛选出选中的
            cartItemVos = cartItems.stream().filter(items -> items.getCheck())
                    .map(item-> {
                        // 更新为最新的价格（查询数据库）
                        BigDecimal price = productFeignService.getPrice(item.getSkuId());
                        item.setPrice(price);
                        return item;
                    }).collect(Collectors.toList());

        }
        return cartItemVos;
    }


}
