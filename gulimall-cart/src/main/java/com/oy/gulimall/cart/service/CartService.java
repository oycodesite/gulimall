package com.oy.gulimall.cart.service;

import com.oy.gulimall.cart.vo.CartItemVo;
import com.oy.gulimall.cart.vo.CartVo;

import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * @Author OY
 * @Date 2021/12/19
 */
public interface CartService {
    /**
     * 获取购物车里面的信息
     * @return
     * @throws Exception
     */
    CartVo getCart() throws  Exception;

    /**
     * 将商品添加至购物车
     * @param skuId
     * @param num
     * @return
     * @throws Exception
     */
    CartItemVo addToCart(Long skuId, Integer num) throws Exception;

    /**
     * 获取购物车某个购物项
     * @param cartKey
     * @return
     */
    List<CartItemVo> getCartItems(String cartKey);

    /**
     * 清空购物车的数据
     * @param cartKey
     */
    void clearCartInfo(String cartKey);

    /**
     * 获取购物车某个购物项
     * @param skuId
     * @return
     */
    CartItemVo getCartItem(Long skuId);

    /**
     * 勾选购物项
     * @param skuId
     * @param checked
     */
    void checkItem(Long skuId, Integer checked);

    /**
     * 改变商品数量
     * @param skuId
     * @param num
     */
    void changeItemCount(Long skuId, Integer num);

    /**
     * 删除商品信息
     * @param skuId
     */
    void deleteIdCartInfo(Integer skuId);

    List<CartItemVo> getUserCartItems();
}
