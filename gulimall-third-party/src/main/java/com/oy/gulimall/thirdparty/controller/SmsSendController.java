package com.oy.gulimall.thirdparty.controller;

import com.oy.gulimall.common.utils.R;
import com.oy.gulimall.thirdparty.componet.SmsComponent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Author OY
 * @Date 2021/10/31
 */
@Controller
@RequestMapping("/sms")
@Slf4j
public class SmsSendController {

    @Autowired
    private SmsComponent smsComponent;

    @ResponseBody
    @GetMapping(value = "/sendCode")
    public R sendCode(@RequestParam("phone") String phone, @RequestParam("code") String code){

        // 发送验证码
        smsComponent.sendCode(phone,code);
        log.info("手机号:{},验证码:{}",phone,code);
        return R.ok();
    }
}
