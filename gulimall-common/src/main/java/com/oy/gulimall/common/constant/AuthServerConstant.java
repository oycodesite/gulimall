package com.oy.gulimall.common.constant;

/**
 * @Author OY
 * @Date 2021/11/1
 */
public class AuthServerConstant {

    public static final String SMS_CODE_CACHE_PREFIX = "sms:code";

    public static final String LOGIN_USER = "loginUser";
}
